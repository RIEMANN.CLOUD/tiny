package club.zhcs.swagger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.TagsSorter;

/**
 * @author kerbores
 */
@Data
@ConfigurationProperties("axe.swagger")
public class SwaggerConfigurationProerties {

    /**
     * 是否开启swagger
     */
    private Boolean enabled;

    /**
     * 标题
     */
    private String title = "";
    /**
     * 描述
     */
    private String description = "";
    /**
     * 版本
     */
    private String version = "";
    /**
     * 许可证
     */
    private String license = "";
    /**
     * 许可证URL
     */
    private String licenseUrl = "";
    /**
     * 服务条款URL
     */
    private String termsOfServiceUrl = "";

    /**
     * 忽略的参数类型
     */
    private List<Class<?>> ignoredParameterTypes = Lang.list();

    /**
     * 联系人
     */
    private Contact contact = new Contact();

    /**
     * swagger会解析的包路径
     */
    private String basePackage = "";

    /**
     * swagger会解析的url规则
     */
    private List<String> basePath = Lang.list();
    /**
     * 在basePath基础上需要排除的url规则
     */
    private List<String> excludePath = Lang.list();

    /**
     * 分组文档
     */
    private Map<String, DocketInfo> docket = new HashMap<>();

    /**
     * host信息
     */
    private String host = "";

    /**
     * 全局参数配置
     */
    private List<GlobalOperationParameter> globalOperationParameters;

    /**
     * 页面功能配置
     */
    private UiConfig uiConfig = new UiConfig();

    /**
     * 是否使用默认预定义的响应消息 ，默认 true
     */
    private Boolean applyDefaultResponseMessages = true;

    /**
     * 全局响应消息
     */
    private GlobalResponseMessage globalResponseMessage;

    /**
     * 全局统一鉴权配置
     */
    private Authorization authorization = new Authorization();

    /**
     * 全局参数设置
     * 
     * @author Kerbores(kerbores@gmail.com) create at 2019-11-21 09:28:51
     */
    @Data
    public static class GlobalOperationParameter {
        /**
         * 参数名
         */
        private String name;

        /**
         * 描述信息
         */
        private String description;

        /**
         * 指定参数类型
         */
        private String modelRef;

        /**
         * 参数放在哪个地方:header,query,path,body.form
         */
        private String parameterType;

        /**
         * 参数是否必须传
         */
        private String required;

    }

    /**
     * 文档标签信息
     * 
     * @author Kerbores(kerbores@gmail.com) create at 2019-11-21 09:29:25
     */
    @Data
    public static class DocketInfo {

        /**
         * 标题
         */
        private String title = "";
        /**
         * 描述
         */
        private String description = "";
        /**
         * 版本
         */
        private String version = "";
        /**
         * 许可证
         */
        private String license = "";
        /**
         * 许可证URL
         */
        private String licenseUrl = "";
        /**
         * 服务条款URL
         */
        private String termsOfServiceUrl = "";
        /**
         * 联系人
         */
        private Contact contact = new Contact();

        /**
         * swagger会解析的包路径
         */
        private String basePackage = "";

        /**
         * swagger会解析的url规则
         */
        private List<String> basePath = Lang.list();
        /**
         * 在basePath基础上需要排除的url规则
         */
        private List<String> excludePath = Lang.list();
        /**
         * 全局参数配置
         */
        private List<GlobalOperationParameter> globalOperationParameters;

        /**
         * 忽略的参数类型
         */
        private List<Class<?>> ignoredParameterTypes = Lang.list();

    }

    /**
     * 联系人
     * 
     * @author Kerbores(kerbores@gmail.com) create at 2019-11-21 09:31:05
     */
    @Data
    public static class Contact {

        /**
         * 联系人
         */
        private String name = "";
        /**
         * 联系人url
         */
        private String url = "";
        /**
         * 联系人email
         */
        private String email = "";

    }

    /**
     * 全局响应信息
     * 
     * @author Kerbores(kerbores@gmail.com) create at 2019-11-21 09:31:48
     */
    @Data
    public static class GlobalResponseMessage {

        /**
         * POST 响应消息体
         */
        List<GlobalResponseMessageBody> post = Lang.list();

        /**
         * GET 响应消息体
         */
        List<GlobalResponseMessageBody> get = Lang.list();

        /**
         * PUT 响应消息体
         */
        List<GlobalResponseMessageBody> put = Lang.list();

        /**
         * PATCH 响应消息体
         */
        List<GlobalResponseMessageBody> patch = Lang.list();

        /**
         * DELETE 响应消息体
         */
        List<GlobalResponseMessageBody> delete = Lang.list();

        /**
         * HEAD 响应消息体
         */
        List<GlobalResponseMessageBody> head = Lang.list();

        /**
         * OPTIONS 响应消息体
         */
        List<GlobalResponseMessageBody> options = Lang.list();

        /**
         * TRACE 响应消息体
         */
        List<GlobalResponseMessageBody> trace = Lang.list();

    }

    /**
     * 全局响应
     * 
     * @author Kerbores(kerbores@gmail.com) create at 2019-11-21 09:32:37
     */
    @Data
    public static class GlobalResponseMessageBody {

        /**
         * 响应码
         */
        private int code;

        /**
         * 响应消息
         */
        private String message;

        /**
         * 响应体
         */
        private String modelRef;

    }

    /**
     * 界面配置
     * 
     * @author Kerbores(kerbores@gmail.com) create at 2019-11-21 09:33:28
     */
    @Data
    public static class UiConfig {

        /**
         * 排序方式
         */
        private String apiSorter = "alpha";

        /**
         * 是否启用json编辑器
         */
        private Boolean jsonEditor = true;
        /**
         * 是否显示请求头信息
         */
        private Boolean showRequestHeaders = true;
        /**
         * 支持页面提交的请求类型
         */
        private String submitMethods = "get,post,put,delete,patch";
        /**
         * 请求超时时间
         */
        private Long requestTimeout = 10000L;

        private Boolean deepLinking;
        private Boolean displayOperationId;
        private Integer defaultModelsExpandDepth;
        private Integer defaultModelExpandDepth;
        private ModelRendering defaultModelRendering;

        /**
         * 是否显示请求耗时，默认false
         */
        private Boolean displayRequestDuration = true;
        /**
         * 可选 none | list
         */
        private DocExpansion docExpansion;
        /**
         * Boolean=false OR String
         */
        private Object filter;
        private Integer maxDisplayedTags;
        private OperationsSorter operationsSorter;
        private Boolean showExtensions;
        private TagsSorter tagsSorter;

        /**
         * Network
         */
        private String validatorUrl;

    }

    /**
     * 认证配置
     * 
     * @author Kerbores(kerbores@gmail.com) create at 2019-11-21 09:35:54
     */
    @Data
    static class Authorization {

        /**
         * 鉴权策略ID，对应 SecurityReferences ID
         */
        private String name = "Authorization";

        /**
         * 鉴权传递的Header参数
         */
        private String keyName = "Authorization";

        /**
         * 需要开启鉴权URL的正则
         */
        private String authRegex = "^.*$";

    }

    private Customer customer = new Customer();

    @Data
    public static class Customer {
        String title = "ZHCS.CLUB 定制Swagger UI";
        String url = "https://www.zhcs.club";
        String logo = "webjars/springfox-swagger-ui/images/logo_small.png";
        Theme theme = Theme.MONOKAI;

        public enum Theme {
            /**
             * 
             */
            FEELING_BLUE("feeling-blue"),
            /**
             * 
             */
            MATERIAL("material"),
            /**
             * 
             */
            MONOKAI("monokai"),
            /**
             * 
             */
            FLATTOP("flattop"),
            /**
             * 
             */
            MUTED("muted"),
            /**
             * 
             */
            NEWSPAPER("newspaper"),
            /**
             * 
             */
            OUTLINE("outline");

            String value;

            private Theme(String value) {
                this.value = value;
            }

            /**
             * @return the value
             */
            public String getValue() {
                return value;
            }

            public static Theme from(String value) {
                return Lang.array2list(values())
                           .stream()
                           .filter(theme -> Strings.equalsIgnoreCase(value, theme.getValue()))
                           .findAny()
                           .orElse(null);
            }
        }
    }

}
