package club.zhcs.swagger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import club.zhcs.swagger.SwaggerConfigurationProerties.Customer;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@RestController
public class SwaggerCustomerController {

    @Autowired
    SwaggerConfigurationProerties config;

    @GetMapping("swagger-customer")
    public Customer customer() {
        return config.getCustomer();
    }

}
