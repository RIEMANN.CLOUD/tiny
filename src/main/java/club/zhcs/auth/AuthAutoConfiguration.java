package club.zhcs.auth;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Kerbores (kerbores@gmail.com)
 *
 */
@Configuration
@EnableConfigurationProperties(AuthAutoConfigurationPeroperties.class)
public class AuthAutoConfiguration {

    @Bean
    @ConditionalOnBean(AuthService.class)
    public TokenAuthInterceptor tokenAuthInterceptor(AuthService authService, AuthAutoConfigurationPeroperties configurationPeroperties) {
        return new TokenAuthInterceptor(authService, configurationPeroperties.getWithoutAuthenticationUrlRegulars());
    }

}
