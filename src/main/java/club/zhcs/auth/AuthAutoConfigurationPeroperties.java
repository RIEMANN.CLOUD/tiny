package club.zhcs.auth;

import java.util.List;

import org.nutz.lang.Lang;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@Data
@ConfigurationProperties("axe.auth")
public class AuthAutoConfigurationPeroperties {
	
    List<String> withoutAuthenticationUrlRegulars = Lang.list();
}
