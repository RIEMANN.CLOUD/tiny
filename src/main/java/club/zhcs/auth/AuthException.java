package club.zhcs.auth;

/**
 * @author Kerbores(kerbores@gmail.com)
 */
public class AuthException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    final String status;

    /**
     * 
     */
    public AuthException() {
        super();
        this.status = "DEFAULT";
    }

    /**
     * 
     * @param message
     *            异常消息
     */
    public AuthException(String message) {
        super(message);
        this.status = "DEFAULT";
    }

    /**
     * 
     * @param message
     *            异常消息
     * @param status
     *            状态
     */
    public AuthException(String message, String status) {
        super(message);
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
