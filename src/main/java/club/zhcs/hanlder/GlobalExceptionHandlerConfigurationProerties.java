package club.zhcs.hanlder;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * @author Kerbores(kerbores@gmail.com)
 */
@Data
@ConfigurationProperties("axe.global.exception")
public class GlobalExceptionHandlerConfigurationProerties {

    /**
     * 是否开启
     */
    private boolean enabled = false;

}
