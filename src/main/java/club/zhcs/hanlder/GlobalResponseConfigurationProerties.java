package club.zhcs.hanlder;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * @author Kerbores(kerbores@gmail.com)
 */
@Data
@ConfigurationProperties("axe.global.response")
public class GlobalResponseConfigurationProerties {

    /**
     * 是否开启
     */
    private boolean enabled = false;

    List<String> ignorePaths;

}
