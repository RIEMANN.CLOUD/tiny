package club.zhcs.apm;

/**
 * @author kerbores
 *
 */
public interface URLProvider {

	String provide();

}
