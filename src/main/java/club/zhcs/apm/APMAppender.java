package club.zhcs.apm;

import java.util.Date;

import org.nutz.json.Json;

import lombok.Builder;
import lombok.Data;

/**
 * @author kerbores
 *
 */
public interface APMAppender {

    @Data
    @Builder
    public static class APMLog {

        String url;

        String tag;

        String user;

        Date actionTime;

        long actionDuration;

        Object[] args;

        Object retuenObj;

        boolean exception;

        Object ext;

        @Override
        public String toString() {
            return Json.toJson(this);
        }

    }

    void append(APMLog log);

    /**
     * @param url
     *            url
     * @param user
     *            当前用户
     * @param log
     *            apm日志
     * @param args
     *            方法参数
     * @param obj
     *            扩展信息
     * @param duration
     *            接口耗时
     * @param exception
     *            是否异常
     * @return 日志信息
     */
    APMLog collect(String url, String user, APM log, Object[] args, Object obj, long duration, boolean exception);
}
