package club.zhcs.utils.codec;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.nutz.repo.Base64;

import lombok.Builder;

/**
 * @author kerbores
 *
 */
@Builder
public class PKCSEncoder {

    private static final String ALGO = "AES";
    private static final String ALGO_MODE = "AES/CBC/NoPadding";

    private String akey;
    private String aiv;

    public String encrypt(String data)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        byte[] iv = toByteArray(aiv);// 因为要求IV为16byte，而此处aiv串为32位字符串，所以将32位字符串转为16byte
        Cipher cipher = Cipher.getInstance(ALGO_MODE);
        int blockSize = cipher.getBlockSize();
        byte[] dataBytes = data.getBytes();
        int plaintextLength = dataBytes.length;
        if (plaintextLength % blockSize != 0) {
            plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
        }
        byte[] plaintext = new byte[plaintextLength];
        System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
        SecretKeySpec keyspec = new SecretKeySpec(akey.getBytes(StandardCharsets.UTF_8), ALGO);
        IvParameterSpec ivspec = new IvParameterSpec(iv);
        cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
        byte[] encrypted = cipher.doFinal(plaintext);
        return Base64.encodeToString(encrypted, false);
    }

    public String decrypt(String encryptedData)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        byte[] encrypted1 = Base64.decode(encryptedData);
        byte[] iv = toByteArray(aiv);
        Cipher cipher = Cipher.getInstance(ALGO_MODE);
        SecretKeySpec keyspec = new SecretKeySpec(akey.getBytes(StandardCharsets.UTF_8), ALGO);
        IvParameterSpec ivspec = new IvParameterSpec(iv);
        cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
        byte[] original = cipher.doFinal(encrypted1);
        String originalString = new String(original);
        return originalString.trim();// 此处添加trim（）是为了去除多余的填充字符，就不用去填充了，具体有什么问题我还没有遇到，有强迫症的同学可以自己写一个PKCS7UnPadding函数
    }

    public static byte[] toByteArray(String hexString) {
        hexString = hexString.toLowerCase();
        final byte[] byteArray = new byte[hexString.length() >> 1];
        int index = 0;
        for (int i = 0; i < hexString.length(); i++) {
            if (index > hexString.length() - 1)
                return byteArray;
            byte highDit = (byte) (Character.digit(hexString.charAt(index), 16) & 0xFF);
            byte lowDit = (byte) (Character.digit(hexString.charAt(index + 1), 16) & 0xFF);
            byteArray[i] = (byte) (highDit << 4 | lowDit & 0xff);
            index += 2;
        }
        return byteArray;
    }

    public static String pkcs7padding(String data) {
        int bs = 16;
        int padding = bs - (data.length() % bs);
        StringBuilder paddingText = new StringBuilder();
        for (int i = 0; i < padding; i++) {
            paddingText.append((char) padding);
        }
        return data + paddingText;
    }
}
