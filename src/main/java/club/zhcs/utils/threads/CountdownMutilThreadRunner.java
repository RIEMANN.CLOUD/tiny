package club.zhcs.utils.threads;

import java.util.concurrent.CountDownLatch;

import lombok.Builder;

@Builder
public class CountdownMutilThreadRunner implements MutilThreadRunner {

	@Override
	public void run(JobRunner jobRunner, int threads) throws InterruptedException {
		CountDownLatch countDownLatch = new CountDownLatch(threads);
		for (int i = 0; i < threads; i++) {
			RunThread.builder().index(i).threads(threads).jobRunner(jobRunner).countDownLatch(countDownLatch).build()
					.start();
		}
		countDownLatch.await();
	}

}
