package club.zhcs.utils.threads;

import org.nutz.log.Logs;

public interface JobRunner {

	public default void run(int index, int threads) {
		Logs.get().debugf("run %d threads current index %d", threads, index);
	}

}
