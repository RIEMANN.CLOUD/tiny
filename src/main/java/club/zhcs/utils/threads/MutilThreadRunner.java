package club.zhcs.utils.threads;

import java.util.concurrent.CountDownLatch;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

public interface MutilThreadRunner {

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode(callSuper = true)
	public static class RunThread extends Thread {

		int index;
		int threads;
		JobRunner jobRunner;
		CountDownLatch countDownLatch;

		@Override
		public void run() {
			jobRunner.run(index, threads);
			if (countDownLatch != null) {
				countDownLatch.countDown();
			}
		}
	}

	public default void run(JobRunner jobRunner, int threads) throws InterruptedException {
		for (int i = 0; i < threads; i++) {
			RunThread.builder().index(i).threads(threads).jobRunner(jobRunner).build().start();
		}
	}
}
