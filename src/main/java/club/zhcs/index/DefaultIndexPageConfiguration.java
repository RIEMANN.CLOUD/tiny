package club.zhcs.index;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(DefaultIndexPageConfigurationProperties.class)
public class DefaultIndexPageConfiguration {

	@Bean
	public DefaultIndexPageController defaultIndexPageController(DefaultIndexPageConfigurationProperties config) {
		return DefaultIndexPageController.builder().url(config.getUrl()).build();
	}

}
