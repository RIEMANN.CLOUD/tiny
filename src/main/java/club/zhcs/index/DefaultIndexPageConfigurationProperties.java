package club.zhcs.index;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties("axe.index")
public class DefaultIndexPageConfigurationProperties {

	String url = "index.html";

}
