package club.zhcs.index;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Controller
public class DefaultIndexPageController {

	String url;

	@GetMapping("/")
	public void index(HttpServletResponse response) throws IOException {
		response.sendRedirect(url);
	}

}
