package tiny.test;

import club.zhcs.utils.threads.CountdownMutilThreadRunner;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		System.err.println(System.currentTimeMillis());
		CountdownMutilThreadRunner.builder().build().run(new TestJonRunner(), 100);
		System.err.println(System.currentTimeMillis());
	}
}
