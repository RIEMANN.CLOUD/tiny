package tiny.test;

import org.nutz.lang.random.R;
import org.nutz.log.Logs;

import club.zhcs.utils.threads.JobRunner;

public class TestJonRunner implements JobRunner {

	@Override
	public void run(int index, int threads) {
		for (int i = 0; i < 100; i++) {
			Logs.get().debugf("run %d threads current index %d, round %d", threads, index, i);
			try {
				Thread.sleep(R.random(1000, 3000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
}
